﻿#include <SFML/Graphics.hpp>
#include <triangle.hpp>
#include <thread>
#include <chrono>
#include <vector>
#include <iostream>

using namespace std::chrono_literals;

int main()
{
    srand(time(0));

    const int width = 1200;
    const int height = 800;
    const int N = 8;
   
    sf::RenderWindow window(sf::VideoMode(width, height), L"2 вариант");
    
    std::vector <ek::Triangle*> triangles;
    for (int i = 45; i <= height; i += height / N)  //создание
    {
        triangles.push_back(new ek::Triangle(45, 30, i, 3, rand() % 8 + 3)); 
    }



    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        for (const auto& triangle : triangles) //движение
        {
            triangle->Move();
            if (triangle->GetX() > width - triangle->GetR())
                triangle->SetX(width - triangle->GetR());
        }

        window.clear();

        for (const auto& triangle : triangles)  //рисование
            window.draw(*triangle->Get());
        window.display();
        std::this_thread::sleep_for(20ms);

    }

    for (const auto& triangle : triangles) //удаление
        delete triangle;
    triangles.clear();


    return 0;
}
