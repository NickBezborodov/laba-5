#include <iostream>
#include "../inc/Header.hpp"

namespace ek {
	void z_mas(int n, int mas[N][N])
	{
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				std::cin >> mas[i][j];
	}

	void Read(int& n, int mas[N][N])
	{
		std::cout << "Введите порядок матрицы: ";
		std::cin >> n;
		std::cout << "Матрица: " << std::endl;
		z_mas(n, mas);
	}



	bool simple(int i, int n, int mas[N][N])
	{
		int j;
		for (j = 0; j < n; j++)
		{
			int k = 0;
			if (mas[i][j] < 2)
				continue;
			for (int d = 2; d <= mas[i][j] - 1; d++)
			{
				if (mas[i][j] % d == 0)
					k = 1;
			}
			if (k == 0)
				return true;
		}
		return false;
	}

	bool sum0(int i, int n, int mas[N][N])
	{
		int j, sum = 0;
		for (j = 0; j < n; j++)
			sum += mas[i][j];
		if (sum == 0)
			return true;
		return false;
	}

	int sum_stolb(int j, int n, int mas[N][N])
	{
		int i, sum = 0;
		for (i = 0; i < n; i++)
			sum += mas[i][j];
		return sum;
	}

	void sort(int n, int mas[N][N])
	{
		for (int j = 0; j < n - 1; j++)
			if (sum_stolb(j, n, mas) < sum_stolb(j + 1, n, mas))
				for (int i = 0; i < n; i++)
				{
					int tmp;
					tmp = mas[i][j];
					mas[i][j] = mas[i][j + 1];
					mas[i][j + 1] = tmp;
				}
	}

	void vyvod(int n, int mas[N][N])
	{
		std::cout << "Итоговая матрица: " << std::endl;
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
				std::cout << mas[i][j] << " ";
			std::cout << std::endl;
		}
	}

	void obr(int n, int mas[N][N])
	{
		for (int i = 0; i < n; i++)
		{
			if ((sum0(i, n, mas)) && (simple(i, n, mas)))
			{
				sort(n, mas);
				break;
			}
		}
	}
}